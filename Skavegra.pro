QT       += core gui svg

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

VERSION = 0.1.0
DEFINES += VERSION_STR=\\\"0.1.0\\\"

TARGET = skavegra
TEMPLATE = app
PATH = /usr/local/share/skavegra


SOURCES += main.cpp\
    mainwindow.cpp \
    svgviewer.cpp

HEADERS  += mainwindow.h \
    svgviewer.h

TRANSLATIONS =  translations/skavegra_de.ts

RESOURCES += \
    resources.qrc

unix {
    target.path = /usr/local/bin
    iconRaster.path = /usr/local/share/icons/hicolor/128x128/apps
    iconRaster.files = gfx/Skavegra.png
    iconVector.path = /usr/local/share/icons/hicolor/scalable/apps
    iconVector.files = gfx/Skavegra.svg
    desktop.path = /usr/local/share/applications
    desktop.files = freedesktop/Skavegra.desktop
    appdata.path = /usr/local/share/appdata
    appdata.files = freedesktop/Skavegra.appdata.xml
    translations.path = /usr/local/share/skavegra/translations
    translations.files = translations/skavegra_de.qm

    INSTALLS += \
        target \
        iconRaster \
        iconVector \
        desktop \
        appdata \
        translations
}
