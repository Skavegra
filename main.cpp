#include "mainwindow.h"
#include <QApplication>
#include <QTranslator>
#include <QPixmap>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setApplicationDisplayName(QApplication::tr("Skavegra"));
    a.setApplicationName(QApplication::tr("Skavegra"));
    a.setApplicationVersion(VERSION_STR);
    a.setWindowIcon(QPixmap(":/gfx/Skavegra.svg"));

    /* Set up translation system */
    QString locale = QLocale::system().name();
    QTranslator translator;
    bool success;
    success = translator.load(QString("translations/skavegra_") + locale);
    if(!success) {
        /* Fall back to system-wide translation file if previous attempt failed */
        success = translator.load(QString("/usr/local/share/skavegra/translations/skavegra_") + locale);
    }
    if(success) {
        a.installTranslator(&translator);
    }

    /* Parse command-line arguments */
    QString firstFileName;
    if(argc >= 2) {
        firstFileName = QString(argv[1]);
    } else {
        firstFileName = QString();
    }

    /* Create main window */
    MainWindow w(0, firstFileName);
    w.show();

    return a.exec();
}
