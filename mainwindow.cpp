#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent, QString firstFileName) : QMainWindow(parent) {
    /* Main window properties */
    setWindowTitle(tr("Skavegra"));
    resize(400, 400);
    hasStatusBar = true;
    hasMenuBar = true;

    /* Docks */
    infoDock = new QDockWidget(tr("Meta&data"), this);
    infoDock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    infoDock->setFeatures(QDockWidget::DockWidgetClosable);
    addDockWidget(Qt::RightDockWidgetArea, infoDock);
    infoDock->hide();

    /* Menu bar */
    programMenu = new QMenu(tr("&Program"), this);
    actionQuit = programMenu->addAction(tr("&Quit"));
    actionQuit->setIcon(QIcon::fromTheme("application-exit"));
    actionQuit->setShortcut(Qt::Key_Q);
    actionQuit->setStatusTip(tr("Close this program"));
    menuBar()->addMenu(programMenu);
    toggleMenuBar(hasMenuBar);

    fileMenu = new QMenu(tr("&File"), this);
    actionOpen = fileMenu->addAction(tr("&Open"));
    actionOpen->setIcon(QIcon::fromTheme("document-open"));
    actionOpen->setStatusTip(tr("Open and display an existing SVG document"));
    actionOpen->setShortcut(Qt::Key_O);
    actionReOpen = fileMenu->addAction(tr("&Reload"));
    actionReOpen->setIcon(QIcon::fromTheme("view-refresh"));
    actionReOpen->setEnabled(false);
    actionReOpen->setShortcut(Qt::Key_R);
    actionReOpen->setStatusTip(tr("Open the currently opened SVG document again to make changes visible"));
    actionClose = fileMenu->addAction(tr("&Close"));
    actionClose->setEnabled(false);
    actionClose->setShortcut(Qt::Key_C);
    actionClose->setStatusTip(tr("Close the currently opened SVG document"));
    menuBar()->addMenu(fileMenu);

    viewMenu = new QMenu(tr("&View"), this);
    actionZoomIn = viewMenu->addAction(tr("Zoom &in"));
    actionZoomIn->setIcon(QIcon::fromTheme("zoom-in"));
    actionZoomIn->setEnabled(false);
    actionZoomIn->setShortcut(Qt::Key_Plus);
    actionZoomIn->setStatusTip(tr("Increase the displaying size of the shown image"));
    actionZoomOut = viewMenu->addAction(tr("Zoom &out"));
    actionZoomOut->setIcon(QIcon::fromTheme("zoom-out"));
    actionZoomOut->setEnabled(false);
    actionZoomOut->setShortcut(Qt::Key_Minus);
    actionZoomOut->setStatusTip(tr("Decrease the displaying size of the shown image"));
    actionZoomReset = viewMenu->addAction(tr("&Reset zoom"));
    actionZoomReset->setIcon(QIcon::fromTheme("zoom-original"));
    actionZoomReset->setEnabled(false);
    actionZoomReset->setShortcut(Qt::Key_0);
    actionZoomReset->setStatusTip(tr("Set the displaying size of the shown image to its default size"));
    actionZoomBestFit = viewMenu->addAction(tr("&Best fit"));
    actionZoomBestFit->setIcon(QIcon::fromTheme("zoom-fit-best"));
    actionZoomBestFit->setEnabled(false);
    actionZoomBestFit->setShortcut(Qt::Key_F);
    actionZoomBestFit->setStatusTip(tr("Automatically adjust the displaying size of the shown image to \
fill the window, with respect to its aspect ratio"));
    viewMenu->addSeparator();
    actionStatusBar = viewMenu->addAction(tr("&Status bar"));
    actionStatusBar->setCheckable(true);
    actionStatusBar->setChecked(true);
    actionStatusBar->setShortcut(Qt::Key_S);
    actionStatusBar->setStatusTip(tr("Show or hide the status bar"));
    actionInfoDock = infoDock->toggleViewAction();
    actionInfoDock->setShortcut(Qt::Key_D);
    actionInfoDock->setStatusTip(tr("Show or hide additional information about the current image"));
    viewMenu->addAction(actionInfoDock);
    menuBar()->addMenu(viewMenu);

    aboutMenu = new QMenu(tr("&About"), this);
    actionAbout = aboutMenu->addAction(tr("&About"));
    actionAbout->setIcon(QIcon::fromTheme("help-about"));
    actionAbout->setStatusTip(tr("Show information about this program"));
    menuBar()->addMenu(aboutMenu);

    /* Status bar */
    setStatusBar(new QStatusBar());
    toggleStatusBar(hasStatusBar);

    /* Central widget: The SVG viewer */
    svgViewer = new SVGViewer();
    setCentralWidget(svgViewer);

    /* Signals and slots */
    connect(actionQuit, SIGNAL(triggered()), this, SLOT(close()));
    connect(actionOpen, SIGNAL(triggered()), this, SLOT(openFileDialog()));
    connect(actionReOpen, SIGNAL(triggered()), this, SLOT(reOpen()));
    connect(actionClose, SIGNAL(triggered()), this, SLOT(closeFile()));
    connect(actionZoomIn, SIGNAL(triggered()), svgViewer, SLOT(zoomIn()));
    connect(actionZoomOut, SIGNAL(triggered()), svgViewer, SLOT(zoomOut()));
    connect(actionZoomReset, SIGNAL(triggered()), svgViewer, SLOT(zoomReset()));
    connect(actionZoomBestFit, SIGNAL(triggered()), this, SLOT(zoomBestFit()));
    connect(actionStatusBar, SIGNAL(toggled(bool)), this, SLOT(toggleStatusBar(bool)));
    connect(actionAbout, SIGNAL(triggered()), this, SLOT(aboutDialog()));

    showStatusBarMessage(QString(tr("Skavegra %1 started.")).arg(VERSION_STR), 5000);

    /* Load first image */
    imageLoaded = false;
    if(!firstFileName.isEmpty()) {
        MainWindow::openFile(firstFileName);
    }

    updateMetadataText();
}

bool MainWindow::reOpen() {
    /* Reset the currently opened document and load and render it again */
    if(lastFileName != NULL) {
        if(MainWindow::openFile(lastFileName)) {
            actionClose->setEnabled(true);
            showStatusBarMessage(QString(tr("File successfully reopened: %1")).arg(lastFileName), 5000);
            return true;
        } else {
            showStatusBarMessage(QString(tr("File could not be reopened: %1")).arg(lastFileName), 5000);
            return false;
        }
    } else {
        return false;
    }
}

void MainWindow::openFileDialog() {
    /* Open the “Open file …” dialog */
    QString fileName;
    fileName = QFileDialog::getOpenFileName(this, tr("Open SVG file …"), ".",
                                            tr("Scalable Vector Graphics") + " (*.svg *.svgz *.svg.gz);; " + tr("All files") + "(*)");
    if (fileName == NULL) {
        /* No file selected */
        return;
    }

    MainWindow::openFile(fileName);
}

bool MainWindow::openFile(QString fileName) {
    /* Delegate file opening to svgViewer */
    if(svgViewer->openFile(fileName)) {
        lastFileName = fileName;
        showStatusBarMessage(QString(tr("File successfully opened: %1")).arg(fileName), 5000);
        actionReOpen->setEnabled(true);
        actionClose->setEnabled(true);
        actionZoomIn->setEnabled(svgViewer->getZoomAndPan());
        actionZoomOut->setEnabled(svgViewer->getZoomAndPan());
        actionZoomReset->setEnabled(svgViewer->getZoomAndPan());
        actionZoomBestFit->setEnabled(svgViewer->getZoomAndPan());
        QString title = svgViewer->getTitle();
        QString dispTitle;
        if(!title.isNull()) {
            dispTitle = title;
        } else {
            dispTitle = tr("(Untitled)");
        }
        setWindowTitle(QString(tr("%1—Skavegra")).arg(dispTitle));
        imageLoaded = true;
        updateMetadataText();
        return true;
    } else {
        showStatusBarMessage(QString(tr("File could not be opened: %1")).arg(fileName), 5000);
        return false;
    }
}

void MainWindow::aboutDialog() {
    QMessageBox::about(this, tr("About Skavegra"),
        "<h1>"
        +QString(tr("Skavegra %1")).arg(VERSION_STR)
        +"</h1><p>"
        +tr("Skavegra is a simple non-conforming viewer for SVG \
(Scalable Vector Graphics) images as specified in the SVG Tiny 1.2 specification.")
        +"</p><p>"
        +tr("This software is not finished yet and is unable to display some \
SVG images correctly."));
}

void MainWindow::closeFile() {
    /* Close the current file and reset the view */
    svgViewer->closeFile();
    actionReOpen->setEnabled(false);
    actionClose->setEnabled(false);
    actionZoomIn->setEnabled(false);
    actionZoomOut->setEnabled(false);
    actionZoomReset->setEnabled(false);
    actionZoomBestFit->setEnabled(false);
    setWindowTitle(QString(tr("Skavegra")));
    showStatusBarMessage(QString(tr("File has been closed.")), 5000);
    imageLoaded = false;
    updateMetadataText();
}

void MainWindow::toggleMenuBar(bool enabled) {
    hasMenuBar = enabled;
    if(!enabled) {
        menuBar()->hide();
    } else {
        menuBar()->show();
    }
}

void MainWindow::toggleStatusBar(bool enabled) {
    hasStatusBar = enabled;
    if(!enabled) {
        statusBar()->hide();
    } else {
        statusBar()->show();
    }
}

void MainWindow::showStatusBarMessage(QString string, int timeout) {
    if(hasStatusBar) {
        statusBar()->showMessage(string, timeout);
    }
}

void MainWindow::updateMetadataText() {
    if(imageLoaded) {
        QString presentedTitle = svgViewer->getTitle();
        QString presentedDesc = svgViewer->getDesc();
        QString presentedAnim = boolToYesNo(svgViewer->getAnim());
        QString presentedZAP;
        QString properties("");
        bool propList = false;
        if(!svgViewer->getZoomAndPan()) {
            presentedZAP = QString(tr("Zooming and panning is disabled"));
            propList = true;
        } else {
            presentedZAP = QString("");
        }
        if(svgViewer->getTitle().isNull()) {
            presentedTitle = "<i>" + tr("Not set") + "</i>";
        } else {
            presentedTitle = svgViewer->getTitle();
        }
        if(svgViewer->getDesc().isNull()) {
            presentedDesc = "<i>" + tr("Not set") + "</i>";
        } else {
            presentedDesc = svgViewer->getDesc();
        }

        if(propList) {
            properties = "<strong>" + tr("Special properties") + "</strong><ul><li>" + presentedZAP + "</li></ul>";
        }

        QLabel* infoText = new QLabel(
        QString(
              "<p><strong>"+tr("Title")+"</strong><br>%1</p>"
            + "<p><strong>"+tr("Description")+"</strong><br>%2</p>"
            + "<p><strong>"+tr("Animations")+"</strong><br>%3</p>"
            + properties
        ).arg(presentedTitle, presentedDesc, presentedAnim));
        infoText->setWordWrap(true);
        infoDock->setWidget(infoText);
    } else {
        QLabel* infoText = new QLabel("<i>"+tr("No image loaded")+"</i>");
        infoText->setWordWrap(true);
        infoDock->setWidget(infoText);
    }
}

void MainWindow::zoomBestFit() {
    QSizeF imgsize = svgViewer->getImageSize();
    if(imgsize.isNull()) {
        return;
    }
    qreal imgw = (qreal) imgsize.width();
    qreal imgh = (qreal) imgsize.height();
    qreal x = -imgw/2;
    qreal y = -imgh/2;

    svgViewer->fitInView(QRectF(x, y, imgw, imgh), Qt::KeepAspectRatio);
}

QString MainWindow::boolToYesNo(bool b) {
    if(b) {
        return QString(tr("Yes"));
    } else {
        return QString(tr("No"));
    }
}
