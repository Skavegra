#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMenu>
#include <QMenuBar>
#include <QAction>
#include <QFileDialog>
#include <QFile>
#include <QErrorMessage>
#include <QMessageBox>
#include <QStatusBar>
#include <QLabel>
#include <QKeySequence>
#include <QDockWidget>

#include "svgviewer.h"

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0, QString firstFileName = QString());
    void updateMetadataText();

private:
    QMenu* programMenu;
    QMenu* fileMenu;
    QMenu* viewMenu;
    QMenu* aboutMenu;
    QAction* actionQuit;
    QAction* actionOpen;
    QAction* actionReOpen;
    QAction* actionClose;
    QAction* actionZoomIn;
    QAction* actionZoomOut;
    QAction* actionZoomReset;
    QAction* actionZoomBestFit;
    QAction* actionStatusBar;
    QAction* actionInfoDock;
    QAction* actionAbout;
    QStatusBar* myStatusBar;
    QErrorMessage* errorFileOpen;
    QDockWidget* infoDock;
    SVGViewer* svgViewer;

    QString lastFileName;
    bool hasStatusBar;
    bool hasMenuBar;
    bool imageLoaded;
    void showStatusBarMessage(QString string, int timeout);
    QString boolToYesNo(bool b);

private slots:
    void openFileDialog();
    bool openFile(QString fileNames);
    bool reOpen();
    void closeFile();
    void aboutDialog();
    void toggleMenuBar(bool enabled);
    void toggleStatusBar(bool enabled);
    void zoomBestFit();
};

#endif // MAINWINDOW_H
