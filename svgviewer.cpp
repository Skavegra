#include "svgviewer.h"

SVGViewer::SVGViewer(QWidget *parent) : QGraphicsView(parent) {
    /* Init scene */
    setScene(new QGraphicsScene(this));
    setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
    setViewportUpdateMode(QGraphicsView::BoundingRectViewportUpdate);
    setOptimizationFlag(QGraphicsView::DontSavePainterState, true);
}

void SVGViewer::paintEvent(QPaintEvent *event) {
    QGraphicsView::paintEvent(event);
}

QString SVGViewer::SVGRemoveWhitespace(QString text) {
    /* Remove whitespace according to SVG spec.
        (xml:space="default")
    */

    /* Remove newlines */
    text = text.remove('\n');
    /* Replace tabs by spaces */
    text.replace("\t", " ");
    text.replace("\v", " ");
    /* Remove leading and traling spaces */
    int i;
    for(i=0; i<text.length(); i++) {
        if(!(text[i] == ' ')) {
            break;
        }
    }
    text.remove(0, i);
    for(i=text.length()-1; i>=0; i--) {
        if(!(text[i] == ' ')) {
            break;
        }
    }
    text.truncate(i+1);
    /* Squash consecutive spaces into a single space */
    int len = text.length();
    int newlen;
    while(len!=newlen) {
        len = text.length();
        text.replace("  ", " ");
        newlen = text.length();
    }

    return text;
}
QString SVGViewer::SVGPreserveWhitespace(QString text) {
    /* Preserve whitespace according to SVG spec.
        (xml:space="preserve") */

    /* Replace newlines and tabs by spaces */
    text.replace("\n", " ");
    text.replace("\t", " ");
    text.replace("\v", " ");

    return text;
}

QString SVGViewer::SVGReadText(QXmlStreamReader* reader, bool keepWhitespace) {
    if(keepWhitespace) {
        return SVGPreserveWhitespace(reader->text().toString());
    } else {
        return SVGRemoveWhitespace(reader->text().toString());
    }
}

bool SVGViewer::openFile(QString fileName) {
    /* Read SVG's XML to gather some metadata and settings. */
    /* FIXME: Only works for non-compressed SVGs */
    QFile xmlFile(fileName);
    if (!xmlFile.open(QFile::ReadOnly)) {
        QMessageBox::critical(this, tr("Error"), tr("The file could not be opened."));
        return false;
    }
    QByteArray bytes = xmlFile.readAll();
    xmlFile.close();
    QXmlStreamReader* xmlReader = new QXmlStreamReader();
    xmlReader->addData(bytes);

    /* Default values for various settings according to SVG spec. */
    zoomAndPan = true;
    title = QString();
    desc = QString();

    /* Waiter variables remember element names */
    bool waiterTitle = false;
    bool waiterDesc = false;
    bool keepWhitespace = false;

    while(!xmlReader->atEnd()) {
        if(xmlReader->readNext()) {
            if(xmlReader->tokenType() == QXmlStreamReader::StartElement) {
                if(xmlReader->namespaceUri() == "http://www.w3.org/2000/svg") {
                    if(xmlReader->name() == "svg") {
                        QString s(xmlReader->attributes().value("zoomAndPan").toString());
                        /* Check if zooming and pan in XML is disabled */
                        if(s == "disable") {
                            zoomAndPan = false;
                        }
                    } else if(xmlReader->name() == "title") {
                        waiterTitle = true;
                    } else if(xmlReader->name() == "desc") {
                        waiterDesc = true;
                    }
                    if(xmlReader->attributes().value("xml:space").toString() == "preserve") {
                        keepWhitespace = true;
                    } else {
                        keepWhitespace = false;
                    }
                }
            } else if(xmlReader->tokenType() == QXmlStreamReader::Characters) {
                if(waiterTitle && title.isNull()) {
                    title = SVGReadText(xmlReader, keepWhitespace);
                    waiterTitle = false;
                } else if(waiterDesc && desc.isNull()) {
                    desc = SVGReadText(xmlReader, keepWhitespace);
                    waiterDesc = false;
                }
            }
        }
    }

    /* Create SVG item and load the file */
    svgItem = new QGraphicsSvgItem(fileName);
    svgRenderer = svgItem->renderer();

    /* Reject invalid SVG documents */
    if (!svgRenderer->isValid()) {
        /* TODO: Also show details in error message */
        QMessageBox::critical(this, tr("Invalid SVG document"),
                              tr("The selected file is not a valid SVG document and could not be opened."));
        return false;
    }

    /* Set up scene */
    if(zoomAndPan) {
        setDragMode(ScrollHandDrag);
    } else {
        setDragMode(NoDrag);
    }

    scene()->clear();
    resetTransform();

    svgItem->setFlags(QGraphicsItem::ItemClipsToShape);
    svgItem->setCacheMode(QGraphicsItem::NoCache);

    /* Show image */
    scene()->addItem(svgItem);
    return true;
}

void SVGViewer::closeFile() {
    delete svgItem;
    svgItem = NULL;
    /* Reset view parameters */
    scene()->clear();
    resetTransform();
    setDragMode(NoDrag);
}

void SVGViewer::zoomIn() {
    if(zoomAndPan && svgItem != NULL) {
        zoomChange(1.25);
    }
}
void SVGViewer::zoomOut() {
    if(zoomAndPan && svgItem != NULL) {
        zoomChange(0.8);
    }
}
void SVGViewer::zoomReset() {
    if(zoomAndPan && svgItem != NULL) {
        resetTransform();
    }
}

void SVGViewer::zoomSet(qreal factor) {
    if(zoomAndPan && svgItem != NULL) {
        zoomReset();
        zoomChange(factor);
    }
}

void SVGViewer::zoomChange(qreal factor) {
    if(zoomAndPan && svgItem != NULL) {
        scale(factor, factor);
    }
}

#ifndef QT_NO_WHEELEVENT
void SVGViewer::wheelEvent(QWheelEvent *event) {
    /* Change zoom on using the mouse wheel */
    qreal factor = qPow(1.2, event->delta() / 240.0);
    zoomChange(factor);
    event->accept();
}
#endif

bool SVGViewer::getZoomAndPan() {
    return zoomAndPan;
}

bool SVGViewer::getAnim() {
    if(svgItem != NULL) {
        return svgRenderer->animated();
    } else {
        return false;
    }
}

QString SVGViewer::getTitle() {
    return title;
}

QString SVGViewer::getDesc() {
    return desc;
}

QSize SVGViewer::getImageSize() {
    if(svgItem != NULL) {
        return svgRenderer->defaultSize();
    } else {
        return QSize();
    }
}
