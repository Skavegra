#ifndef SVGVIEWER_H
#define SVGVIEWER_H

#include <QSvgRenderer>
#include <QGraphicsView>
#include <QGraphicsSvgItem>
#include <QWheelEvent>
#include <QMessageBox>
#include <QXmlStreamReader>
#include <qmath.h>

class SVGViewer : public QGraphicsView {
    Q_OBJECT

public:
    SVGViewer(QWidget *parent = 0);
    bool getZoomAndPan();
    bool getAnim();
    QString getTitle();
    QString getDesc();
    QSize getImageSize();

public slots:
    bool openFile(QString fileName);
    void closeFile();
    void zoomIn();
    void zoomOut();
    void zoomReset();
    void zoomSet(qreal factor);

protected:
    void paintEvent(QPaintEvent* event);
#ifndef QT_NO_WHEELEVENT
    void wheelEvent(QWheelEvent* event);
#endif

private slots:
    void zoomChange(qreal factor);

private:
    QGraphicsSvgItem* svgItem;
    QSvgRenderer* svgRenderer;
    bool zoomAndPan;
    QString title;
    QString desc;

    QString SVGPreserveWhitespace(QString text);
    QString SVGRemoveWhitespace(QString text);
    QString SVGReadText(QXmlStreamReader* reader, bool keepWhitespace);

};

#endif // SVGVIEWER_H
