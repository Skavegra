<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE" sourcelanguage="en_GB">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="5"/>
        <location filename="../mainwindow.cpp" line="191"/>
        <source>Skavegra</source>
        <translation>Skavegra</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="11"/>
        <source>Meta&amp;data</source>
        <translation>Meta&amp;daten</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="18"/>
        <source>&amp;Program</source>
        <translation>&amp;Programm</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="19"/>
        <source>&amp;Quit</source>
        <translation>&amp;Beenden</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="22"/>
        <source>Close this program</source>
        <translation>Dieses Programm schließen</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="26"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="27"/>
        <source>&amp;Open</source>
        <translation>&amp;Öffnen</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="29"/>
        <source>Open and display an existing SVG document</source>
        <translation>Ein bestehendes SVG-Dokument öffnen und anzeigen</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="31"/>
        <source>&amp;Reload</source>
        <translation>&amp;Neu laden</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="35"/>
        <source>Open the currently opened SVG document again to make changes visible</source>
        <translation>Das derzeit geöffnete SVG-Dokument erneut öffnen, um Änderungen sichtbar zu machen</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="36"/>
        <source>&amp;Close</source>
        <translation>&amp;Schließen</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="39"/>
        <source>Close the currently opened SVG document</source>
        <translation>Das derzeit geöffnete SVG-Dokument schließen</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="42"/>
        <source>&amp;View</source>
        <translation>&amp;Ansicht</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="43"/>
        <source>Zoom &amp;in</source>
        <translation>Ver&amp;größern</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="47"/>
        <source>Increase the displaying size of the shown image</source>
        <translation>Die Anzeigegröße des angezeigten Bildes erhöhen</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="48"/>
        <source>Zoom &amp;out</source>
        <translation>Ver&amp;kleinern</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="52"/>
        <source>Decrease the displaying size of the shown image</source>
        <translation>Die Anzeigegröße des angezeigten Bildes verringern</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="53"/>
        <source>&amp;Reset zoom</source>
        <translation>&amp;Originalgröße</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="57"/>
        <source>Set the displaying size of the shown image to its default size</source>
        <translation>Die Anzeigegröße des gezeigten Bildes auf die Standardgröße setzen</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="58"/>
        <source>&amp;Best fit</source>
        <translation>G&amp;röße an Fenster anpassen</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="62"/>
        <source>Automatically adjust the displaying size of the shown image to fill the window, with respect to its aspect ratio</source>
        <translation>Automatisch die Anzeigegröße des angezeigten Bildes anpassen, um das Fenster zu füllen</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="65"/>
        <source>&amp;Status bar</source>
        <translation>&amp;Statusleiste</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="69"/>
        <source>Show or hide the status bar</source>
        <translation>Die Statusleiste zeigen oder verbergen</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="72"/>
        <source>Show or hide additional information about the current image</source>
        <translation>Zusatzinformationen über das aktuelle Bild anzeigen oder verbergen</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="76"/>
        <location filename="../mainwindow.cpp" line="77"/>
        <source>&amp;About</source>
        <translation>&amp;Über</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="79"/>
        <source>Show information about this program</source>
        <translation>Informationen über dieses Programm anzeigen</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="102"/>
        <source>Skavegra 0.1.0 started.</source>
        <translation>Skavegra 0.1.0 gestartet.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="118"/>
        <source>File successfully reopened: %1</source>
        <translation>Datei erfolgreich neu geladen: %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="121"/>
        <source>File could not be reopened: %1</source>
        <translation>Datei konnte nicht erneut geöffnet werden: %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="132"/>
        <source>Open SVG file …</source>
        <translation>SVG-Datei öffnen …</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="133"/>
        <source>Scalable Vector Graphics</source>
        <translation>Scalable Vector Graphics</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="133"/>
        <source>All files</source>
        <translation>Alle Dateien</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="146"/>
        <source>File successfully opened: %1</source>
        <translation>Datei erfolgreich geöffnet: %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="158"/>
        <source>(Untitled)</source>
        <translation>(Ohne Titel)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="160"/>
        <source>%1—Skavegra</source>
        <translation>%1 – Skavegra</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="165"/>
        <source>File could not be opened: %1</source>
        <translation>Datei konnte nicht geöffnet werden: %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="171"/>
        <source>About Skavegra</source>
        <translation>Über Skavegra</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="173"/>
        <source>Skavegra 0.1.0</source>
        <translation>Skavegra 0.1.0</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="175"/>
        <source>Skavegra is a simple non-conforming viewer for SVG (Scalable Vector Graphics) images as specified in the SVG Tiny 1.2 specification.</source>
        <translation>Skavegra ist ein einfacher nichtkonformer Betrachter für SVG-Bilder (Scalable Vector Graphics) nach der »SVG Tiny 1.2«-Spezifikation.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="178"/>
        <source>This software is not finished yet and is unable to display some SVG images correctly.</source>
        <translation>Diese Software ist noch nicht fertig und ist unfähig, einige SVG-Bilder korrekt anzuzeigen.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="192"/>
        <source>File has been closed.</source>
        <translation>Datei wurde geschlossen.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="230"/>
        <source>Zooming and panning is disabled</source>
        <translation>Vergrößern und Verschieben ist deaktiviert</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="236"/>
        <location filename="../mainwindow.cpp" line="241"/>
        <source>Not set</source>
        <translation>Nicht gesetzt</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="247"/>
        <source>Special properties</source>
        <translation>Besondere Eigenschaften</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="252"/>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="253"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="254"/>
        <source>Animations</source>
        <translation>Animationen</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="260"/>
        <source>No image loaded</source>
        <translation>Kein Bild geladen</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="281"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="283"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
</context>
<context>
    <name>SVGViewer</name>
    <message>
        <location filename="../svgviewer.cpp" line="75"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../svgviewer.cpp" line="75"/>
        <source>The file could not be opened.</source>
        <translation>Die Datei konnte nicht geöffnet werden.</translation>
    </message>
    <message>
        <location filename="../svgviewer.cpp" line="133"/>
        <source>Invalid SVG document</source>
        <translation>Ungültiges SVG-Dokument</translation>
    </message>
    <message>
        <location filename="../svgviewer.cpp" line="134"/>
        <source>The selected file is not a valid SVG document and could not be opened.</source>
        <translation>Die gewählte Datei ist kein gültiges SVG-Dokument und konnte nicht geöffnet werden.</translation>
    </message>
</context>
</TS>
